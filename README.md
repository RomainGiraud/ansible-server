# Ansible playbooks for server installation

## Playbooks

To install a new server, you must have in your `~/.ssh/config` a `server` host.

First time, you must run the following playbook. It will set the right ssh port on the server.
```bash
ansible-playbook config-ssh-port.yml --vault-password-file=pwd_file.txt
```

Then you can run the install playbook:
```bash
ansible-playbook install.yml --vault-password-file=pwd_file.txt [--check]
```

To edit protected variables:
```bash
ansible-vault edit vars.yml
```

## Tests

To test with Vagrant:
```bash
vagrant up --no-provision
vagrant provision
vagrant destroy
```

## Fail2Ban

To get Fail2Ban database:
```bash
fail2ban-client get dbfile
```

To get number of banned ips:
```sql
select count(ip),bancount from bans group by bancount order by bancount desc;
```

## Add a site

* git init .
* Add lfs filters: git lfs track '*.doc' '*.pdf' '*.png' '*.jpg' '*.jpeg' '*.gif'
* Made a first commit to push it to git (git status | grep -v -E '\.(php|xml|md|json|js|css|png|gif|txt|jpg|jpeg|ttf|woff|svg|eot|pdf|scss|mo|po|html|pot)\"?$'
)
* Add gitignore for maintenance scripts (.tools)
* Add configuration in playbook
* Configure gitlab for the first push (lafav backup user must be maintainer)
* Then change gitlab configuration for protected branch master (dev can only push)

## TODO

* [ ] set ACL for `/home/http` and `/home/http/portfolio` (recursively)
* [ ] add a weekly mail to summarize banned ips
* [ ] add a mail to ask to update the system
* [ ] backup everything on gitlab
* [ ] changed default port for ssh
* [ ] activer selinux
