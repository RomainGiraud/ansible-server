def getbackup(l):
  ret = list()
  for e in l:
    item = e['item']
    f = sorted(e['files'], key=lambda k: k['path'])[0]['path']
    item['file'] = f
    ret.append(item)
  return ret

class FilterModule(object):
  def filters(self):
    return { 'getbackup': getbackup }
