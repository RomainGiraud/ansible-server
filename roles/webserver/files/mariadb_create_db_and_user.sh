#!/bin/bash

PWD=$1

DBNAME=$2
DBUSER=$3
DBPWD=$4

exists=$(mysql -uroot -p$PWD -B --disable-column-names -e "SELECT 1 FROM mysql.user WHERE user='$DBUSER'")
if [[ x"$exists" != "x1" ]]; then
    mysql -uroot -p$PWD << EOF
    CREATE USER '$DBUSER'@'localhost' IDENTIFIED BY '$DBPWD';
    GRANT ALL PRIVILEGES ON $DBNAME.* TO '$DBUSER'@'localhost';
    FLUSH PRIVILEGES;
EOF
fi

exists=$(mysql -uroot -p$PWD -B --disable-column-names -e "SELECT 1 FROM information_schema.schemata WHERE schema_name='$DBNAME'")
if [[ x"$exists" != "x1" ]]; then
    mysql -uroot -p$PWD << EOF
    CREATE DATABASE $DBNAME;
EOF
fi

mysql -uroot -p$PWD << EOF
GRANT ALL PRIVILEGES ON $DBNAME.* TO '$DBUSER'@'localhost';
FLUSH PRIVILEGES;
EOF