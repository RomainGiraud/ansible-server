#!/bin/bash

MYSQL_ROOT_PASSWORD=$1

SECURE_MYSQL=$(expect -c "

set timeout 10
spawn mysql_secure_installation

expect \"Enter current password for root (enter for none): \"
send \"\n\"

expect {
    \"Access denied for user 'root'@'localhost' (using password: NO)\" abort
    \"Set root password? \[Y/n\] \"
}
send \"y\n\"

expect \"New password: \"
send \"$MYSQL_ROOT_PASSWORD\n\"

expect \"Re-enter new password: \"
send \"$MYSQL_ROOT_PASSWORD\n\"

expect \"Remove anonymous users? \[Y/n\] \"
send \"y\n\"

expect \"Disallow root login remotely? \[Y/n\] \"
send \"y\n\"

expect \"Remove test database and access to it? \[Y/n\] \"
send \"y\n\"

expect \"Reload privilege tables now? \[Y/n\] \"
send \"y\n\"

expect eof
")

echo "$SECURE_MYSQL"
